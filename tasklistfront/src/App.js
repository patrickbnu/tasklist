import React, { Component } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css'
import update from 'immutability-helper'
import Board from 'react-trello/dist'

class App extends Component {

	state = {
    	
    	eventBus : undefined,
        loaded: false,
        items: [],
        item: null,
        data: {
            lanes: [{
                    status: 0,
                    id: 0,
                    title: 'To do',
                    cards: [],
                    style: {
                        backgroundColor: '#ffb3b3fc'
                    }
                },
                {
                    status: 1,
                    id: 1,
                    title: 'Doing',
                    cards: [],
                    disallowAddingCard: true,
                    style: {
                        backgroundColor: '#f3eda0'
                    }
                },
                {
                    status: 2,
                    id: 2,
                    title: 'Done',
                    cards: [],
                    disallowAddingCard: true,
                    style: {
                        backgroundColor: '#c2fbb4fc'
                    }
                }
            ]
        }
    }

    //workarround to keep the new cards generated from api
    newCards =  []

    // get a card from their id
    getCardById(id) {
        let cardReturn = null;

        this.state.data.lanes.forEach(function(lane, lIndex) {

            lane.cards.forEach(function(card, cIndex) {
                if (card.id === id){ 
                    cardReturn = card			        
			    }
            })
        })

        //return data.lanes.map(e => e.cards).reduce((full, e) => (full.push(...e), full), []).filter(e => e.id === id)
        
        return cardReturn;
    }

    // when interface its ok, get all tasks from server
    componentDidMount() {
        fetch('/task/all')
            .then(response => response.json())
            .then((items) => {

                let state = this.state.data;
                state.lanes.forEach(function (lane){
                	lane.cards = []
                })

                items.forEach(function(item) {
                    state.lanes[item.status].cards.push({
                        id: item.id,
                        title: item.title,
                        description: item.description
                    });
                })

                this.setState({
                    data: state,
                    loaded: true
                })
            })
            .catch(err => console.log(err))
    }

    // remove a task from server
    remove(id) {
        fetch('/task?id=' + id , {
            method: 'DELETE'
        });
    }

    /* submit to server.
     *    - POST when creating a new card
     *    - PUT when moving a card, or editing a card (edit not implemented yed)
     */
    async handleSubmit(item, generatedFrontId) {    	

        await fetch('/task' + (item.id ? '?id=' + item.id : ''), {
            method: (item.id) ? 'PUT' : 'POST',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify(item)
        })
        .then(response => response.json())
        .then((newItem) => {       	
        	this.newCards[generatedFrontId] = {...item, id : newItem.id}   
        	//TODO need to add this new card to the state
		})
        .catch(err => console.log(err))
    }

    render() {

    	// after delete click
        const handleCardDelete = (cardId, laneId) => {
            this.remove(cardId);
        }

        // after drag&drop card
        const handleCardMoveAcrossLanes = (fromLaneId, toLaneId, cardId, index) => {
            let card = this.getCardById(cardId);

            if (typeof cardId === 'string'){
            	// because the new cards are not in state.
            	this.handleSubmit({...this.newCards[cardId], status: toLaneId
	            })
            } else {
	            this.handleSubmit({...card,id:  cardId,status: toLaneId})
	        }

            let newData = update(this.state.data, {lanes: {$set: this.state.data.lanes}})
            this.setState({data: newData})

        }

        // after add card click
        const handleCardAdd = (card, laneId) => {
			this.handleSubmit({...card, status: 0, id: undefined}, card.id)
        }


        const {data,loaded} = this.state

        if (!loaded) {
            return <div > Loading... < /div>;
        } else {
            return ( <
                Board
                data = {data}
                style = {{backgroundColor: '#blue'}}
                draggable id = "EditableBoard1"
               	onCardDelete = {handleCardDelete}
                onCardMoveAcrossLanes = {handleCardMoveAcrossLanes}
                onCardAdd = {handleCardAdd}
                editable /
                >
            )
        }
    }


}

export default App