package nascimento.patrick.tasklist.model.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import nascimento.patrick.tasklist.model.Task;

public interface TaskRespository extends CrudRepository<Task, Integer> {
	

	@Modifying
	@Query("update Task t set t.status = ?2, t.title = ?3, t.description = ?4 where t.id = ?1")
	@Transactional
	void updateTaskStatus(Integer id, Integer status, String title, String description);

}
