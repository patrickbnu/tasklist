package nascimento.patrick.tasklist.constroller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import nascimento.patrick.tasklist.model.Task;
import nascimento.patrick.tasklist.model.repository.TaskRespository;

/**
 * Crud to Task
 * 
 * @author Patrick
 *
 */
@Controller
@RequestMapping(path = "/task")
public class TaskController {
	
	@Autowired
	private TaskRespository taskRepository;

	@GetMapping(path = "/all")
	public @ResponseBody Iterable<Task> getAllTasks() {
		return taskRepository.findAll();
	}
	
	
	@GetMapping
	public @ResponseBody Task getTask(@RequestParam Integer id) {
		return taskRepository.findById(id).get();
	}
	
	@PostMapping
	public @ResponseBody Task addTask(@RequestBody Task task) {
		return taskRepository.save(task);
	}
	
	@PutMapping
	public @ResponseBody Task updateTask(@RequestParam Integer id, @RequestBody Task task) { 
		taskRepository.updateTaskStatus(id, task.getStatus(), task.getTitle(), task.getDescription());
		return taskRepository.findById(task.getId()).get();
	}
	
	@DeleteMapping
	public @ResponseStatus void deleteTask(@RequestParam Integer id) {
		taskRepository.deleteById(id);
	}
	
}
